﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace CBGames
{
    public class EffectManager : MonoBehaviour
    {

        public static EffectManager Instance { private set; get; }

        [SerializeField] private FadingCubeController fadingCubeControllerPrefab = null;
        [SerializeField] private ParticleSystem mergeCubeExplodeEffectPrefab = null;

        private List<FadingCubeController> listFadingCubeController = new List<FadingCubeController>();
        private List<ParticleSystem> listMergeCubeExplodeEffect = new List<ParticleSystem>();

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(Instance.gameObject);
                Instance = this;
            }
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }


        /// <summary>
        /// Play the given particle then disable it 
        /// </summary>
        /// <param name="par"></param>
        /// <returns></returns>
        private IEnumerator CRPlayParticle(ParticleSystem par)
        {
            par.Play();
            yield return new WaitForSeconds(2f);
            par.gameObject.SetActive(false);
        }



        /// <summary>
        /// Play a merge cube explode at given position.
        /// </summary>
        /// <param name="pos"></param>
        public void PlayMergeCubeExplodeEffect(Vector3 pos)
        {
            //Find in the list
            ParticleSystem cubeSmoke = listMergeCubeExplodeEffect.Where(a => !a.gameObject.activeInHierarchy).FirstOrDefault();

            if (cubeSmoke == null)
            {
                //Didn't find one -> create new one
                cubeSmoke = Instantiate(mergeCubeExplodeEffectPrefab, pos, Quaternion.identity).GetComponent<ParticleSystem>();
                cubeSmoke.gameObject.SetActive(false);
                listMergeCubeExplodeEffect.Add(cubeSmoke);
            }

            cubeSmoke.transform.position = pos;
            cubeSmoke.gameObject.SetActive(true);
            StartCoroutine(CRPlayParticle(cubeSmoke));
        }


        /// <summary>
        /// Create a FadingCubeController object at the position of given parent.
        /// </summary>
        /// <param name="parent"></param>
        public void CreateFadingCubeAndFade(Transform parent)
        {
            //Find in the list
            FadingCubeController fadingCubeController = listFadingCubeController.Where(a => !a.gameObject.activeInHierarchy).FirstOrDefault();

            if (fadingCubeController == null)
            {
                //Didn't find one -> create new one
                fadingCubeController = Instantiate(fadingCubeControllerPrefab, Vector3.zero, Quaternion.identity);
                listFadingCubeController.Add(fadingCubeController);
            }

            fadingCubeController.gameObject.SetActive(true);
            fadingCubeController.transform.SetParent(parent);
            fadingCubeController.StartFade();
        }
    }

}