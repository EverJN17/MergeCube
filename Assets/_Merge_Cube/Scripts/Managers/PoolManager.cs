﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;


namespace CBGames
{
    public class PoolManager : MonoBehaviour
    {
        public static PoolManager Instance { private set; get; }

        [SerializeField] private MergeCubeController[] mergeCubeControllerPrefabs = null;

        private List<MergeCubeController> listMergeCubeController = new List<MergeCubeController>();
        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(Instance.gameObject);
                Instance = this;
            }
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }



        /// <summary>
        /// Set up color for a merge cube prefab base on given type.
        /// </summary>
        /// <param name="indexType"></param>
        /// <param name="color"></param>
        public void SetUpColorForMergeCube(IndexType indexType, Color color)
        {
            MergeCubeController prefab = mergeCubeControllerPrefabs.Where(a => a.IndexType.Equals(indexType)).FirstOrDefault();
            prefab.SetUpColor(color);
        }





        /// <summary>
        /// Get an inactive MergeCubeController object.
        /// </summary>
        /// <returns></returns>
        public MergeCubeController GetMergeCubeController(IndexType indexType)
        {
            //Find in the list
            MergeCubeController mergeCubeController = listMergeCubeController.Where(a => !a.gameObject.activeInHierarchy && a.IndexType.Equals(indexType)).FirstOrDefault();

            if (mergeCubeController == null)
            {
                //Did not find one -> create new one
                MergeCubeController prefab = mergeCubeControllerPrefabs.Where(a => a.IndexType.Equals(indexType)).FirstOrDefault();
                mergeCubeController = Instantiate(prefab, Vector3.zero, Quaternion.identity);
                mergeCubeController.gameObject.SetActive(false);
                listMergeCubeController.Add(mergeCubeController);
            }

            return mergeCubeController;
        }

    }

}