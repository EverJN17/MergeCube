﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CBGames
{
    public class IngameManager : MonoBehaviour
    {

        public static IngameManager Instance { private set; get; }
        public static event System.Action<IngameState> GameStateChanged = delegate { };
        public static EnvironmentType EnvironmentType { private set; get; }
        public IngameState IngameState
        {
            get
            {
                return ingameState;
            }
            private set
            {
                if (value != ingameState)
                {
                    ingameState = value;
                    GameStateChanged(ingameState);
                }
            }
        }

        [Header("Enter a number of level to test. Set back to 0 to disable this feature.")]
        [SerializeField] private int testingLevel = 0;

        [Header("Ingame Config")]
        [SerializeField] private float reviveWaitTime = 5f;
        [SerializeField] private MergeCubeColorConfig[] mergeCubeColorConfigs = null;

        [Header("Levels Config")]
        [SerializeField] private List<LevelConfig> listLevelConfig = null;

        [Header("Ingame References")]
        [SerializeField] private Transform completedLevelEffectsTrans = null;
        [SerializeField] private Transform fadingLineTrans = null;
        [SerializeField] private Transform finishWallTrans = null;
        [SerializeField] private Material backgroundMaterial = null;
        [SerializeField] private ParticleSystem[] completedLevelEffects = null;
        [SerializeField] private Transform[] mergeCubePositions = null;
        [SerializeField] private EnvironmentConfig[] environmnetConfigs = null;

        public IndexType HighestTargetIndexType { private set; get; }
        public float ReviveWaitTime { get { return reviveWaitTime; } }
        public float FinishWallZPos { get { return finishWallTrans.position.z; } }
        public int CurrentLevel { private set; get; }
        public bool IsRevived { private set; get; }



        private IngameState ingameState = IngameState.Ingame_GameOver;
        private SoundClip background = null;
        private LevelConfig currentLevelConfig = null;
        private List<TargetData> listTargetData = new List<TargetData>();
        private MergeCubeController currentMergeCubeController = null;
        private Vector3 savedMergeCubePos = Vector3.zero;
        private float timeToCompleteLevel = 0;
        private float firstX = 0;
        private float firstY = 0;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                DestroyImmediate(Instance.gameObject);
                Instance = this;
            }
        }

        private void OnDestroy()
        {
            if (Instance == this)
            {
                Instance = null;
            }
        }

        private void Start()
        {
            Application.targetFrameRate = 60;
            ViewManager.Instance.OnLoadingSceneDone(SceneManager.GetActiveScene().name);

            //Setup variables
            IsRevived = false;
            completedLevelEffectsTrans.gameObject.SetActive(false);
            fadingLineTrans.gameObject.SetActive(false);

            //Set current level
            if (!PlayerPrefs.HasKey(PlayerPrefsKey.SAVED_LEVEL_PPK))
            {
                PlayerPrefs.SetInt(PlayerPrefsKey.SAVED_LEVEL_PPK, 1);
            }


            //Setup color for all merge cubes
            foreach(MergeCubeColorConfig o in mergeCubeColorConfigs)
            {
                PoolManager.Instance.SetUpColorForMergeCube(o.IndexType, o.Color);
            }

            //Setup table
            foreach (EnvironmentConfig o in environmnetConfigs)
            {
                if (o.EnvironmentType == EnvironmentType)
                {
                    o.TableObject.SetActive(true);
                }
                else
                {
                    o.TableObject.SetActive(false);
                }
            }

            //Load level parameters
            CurrentLevel = (testingLevel != 0) ? testingLevel : PlayerPrefs.GetInt(PlayerPrefsKey.SAVED_LEVEL_PPK);
            foreach (LevelConfig levelConfig in listLevelConfig)
            {
                if (levelConfig.MinLevel <= CurrentLevel && CurrentLevel < levelConfig.MaxLevel)
                {
                    currentLevelConfig = levelConfig;

                    //Setup parameters
                    timeToCompleteLevel = UnityEngine.Random.Range(currentLevelConfig.MinTimeToCompleteLevel, currentLevelConfig.MaxTimeToCompleteLevel);
                    background = currentLevelConfig.MusicClip;
                    backgroundMaterial.SetColor("_TopColor", currentLevelConfig.BackgroundTopColor);
                    backgroundMaterial.SetColor("_BottomColor", currentLevelConfig.BackgroundBottomColor);

                    //Create initial merge cubes
                    List<Transform> listInitialTrans = new List<Transform>();
                    foreach (InitialMergeCubeConfig a in levelConfig.InitialMergeCubeConfigs)
                    {
                        for(int i = 0; i < a.Quantity; i++)
                        {
                            Transform initialTrans = mergeCubePositions[UnityEngine.Random.Range(0, mergeCubePositions.Length)];
                            while (listInitialTrans.Contains(initialTrans))
                            {
                                initialTrans = mergeCubePositions[UnityEngine.Random.Range(0, mergeCubePositions.Length)];
                            }
                            listInitialTrans.Add(initialTrans);

                            MergeCubeController mergeCubeController = PoolManager.Instance.GetMergeCubeController(a.IndexType);
                            mergeCubeController.transform.position = initialTrans.position;
                            mergeCubeController.transform.localEulerAngles = Vector3.zero;
                            mergeCubeController.gameObject.SetActive(true);

                            if (listInitialTrans.Count == 11)
                                break;
                        }

                        if (listInitialTrans.Count == 11)
                            break;
                    }


                    //Define the number of target
                    int numberOfTarget = (levelConfig.NumberOfTarget <= levelConfig.TargetIndexTypes.Length) ? levelConfig.NumberOfTarget : levelConfig.TargetIndexTypes.Length;

                    //Create the list of IndexType for all the targets
                    List<IndexType> listTargetIndexType = new List<IndexType>();
                    while (listTargetIndexType.Count < numberOfTarget)
                    {
                        IndexType indexType = levelConfig.TargetIndexTypes[UnityEngine.Random.Range(0, levelConfig.TargetIndexTypes.Length)];
                        while (listTargetIndexType.Contains(indexType))
                        {
                            indexType = levelConfig.TargetIndexTypes[UnityEngine.Random.Range(0, levelConfig.TargetIndexTypes.Length)];
                        }

                        if (listTargetIndexType.Count == 0)
                            listTargetIndexType.Add(indexType);
                        else
                        {
                            for (int i = 0; i < listTargetIndexType.Count; i++)
                            {
                                int inListIndexConvert = int.Parse(listTargetIndexType[i].ToString().Split('_')[1]);
                                int currentIndexConvert = int.Parse(indexType.ToString().Split('_')[1]);
                                if (currentIndexConvert < inListIndexConvert)
                                {
                                    //Insert data at the current elemt which is i element.
                                    listTargetIndexType.Insert(i, indexType);
                                    break;
                                }
                            }

                            if (!listTargetIndexType.Contains(indexType))
                            {
                                listTargetIndexType.Add(indexType);
                            }
                        }
                    }


                    //Adding elements for listTargetData
                    int highestTargetIndexTypeConvert = 0;
                    for(int i = 0; i < listTargetIndexType.Count; i++)
                    {
                        TargetData targetData = new TargetData();
                        targetData.SetIndexType(listTargetIndexType[i]);
                        targetData.SetQuantity(UnityEngine.Random.Range(levelConfig.MinQuantity, levelConfig.MaxQuantity));
                        listTargetData.Add(targetData);

                        int indexTypeConvert = int.Parse(listTargetIndexType[i].ToString().Split('_')[1]);
                        if (indexTypeConvert > highestTargetIndexTypeConvert)
                        {
                            highestTargetIndexTypeConvert = indexTypeConvert;
                            HighestTargetIndexType = listTargetIndexType[i];
                        }
                    }
                }
            }

            PlayingGame();
        }



        private void Update()
        {
            if (ingameState == IngameState.Ingame_Playing && currentMergeCubeController != null)
            {
                if (currentMergeCubeController.Rigidbody.isKinematic) //Move the cube
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        firstX = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)).x;
                    }
                    else if (Input.GetMouseButton(0))
                    {
                        float currentX = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)).x;
                        float distanceX = currentX - firstX;
                        Vector3 newPos = currentMergeCubeController.transform.position;
                        newPos += new Vector3(distanceX, 0, 0);
                        newPos.x = Mathf.Clamp(newPos.x, -3f, 3f);
                        currentMergeCubeController.transform.position = newPos;
                        firstX = currentX;
                    }

                    if (Input.GetMouseButtonUp(0))
                    {
                        currentMergeCubeController.Rigidbody.isKinematic = false;
                    }
                }
                else if (!currentMergeCubeController.Rigidbody.isKinematic) //Handle action of the cube (force foward or fly forward)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        firstY = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)).y;
                    }
                    else if (Input.GetMouseButtonUp(0))
                    {
                        ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.shootMergeCube);
                        float currentY = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)).y;
                        float distance = currentY - firstY;
                        if (currentY > firstY && distance > 0.2f)
                        {
                            //User swiped from top to bottom -> force the current merge cube flying forward
                            ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.shootMergeCube);
                            float flyingDistance = (distance > 2f) ? (distance + 4f) : (distance + 6f);
                            flyingDistance = Mathf.Clamp(flyingDistance, 0, 9f);
                            currentMergeCubeController.Rigidbody.velocity += (Vector3.forward + Vector3.up * 1.75f) * flyingDistance;
                            currentMergeCubeController.Rigidbody.AddTorque((Vector3.right) * 50f);
                        }
                        else //User swiped from bottom to top -> force the current merge cube moving forward
                        {
                            ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.shootMergeCube);
                            currentMergeCubeController.Rigidbody.velocity = Vector3.forward * 30f;
                        }

                        fadingLineTrans.transform.SetParent(null);
                        fadingLineTrans.gameObject.SetActive(false);
                        currentMergeCubeController = null;
                        StartCoroutine(CRWaitAndCreateAnotherMergeCube());
                    }
                }
            }
        }




        /// <summary>
        /// Actual start the game (call Ingame_Playing event).
        /// </summary>
        public void PlayingGame()
        {
            //Fire event
            IngameState = IngameState.Ingame_Playing;
            ingameState = IngameState.Ingame_Playing;

            //Other actions

            if (IsRevived)
            {
                ResumeBackgroundMusic(0.5f);

                //Disable the currentMergeCubeController
                if (currentMergeCubeController != null)
                    currentMergeCubeController.gameObject.SetActive(false);

                //Create another cube for the table and start counting time.
                CreateMergeCubeForTheTable();
                StartCoroutine(CRDecreasingTimeToCompleteLevel());
            }
            else
            {
                PlayBackgroundMusic(0.5f);
                CreateMergeCubeForTheTable();
                ViewManager.Instance.IngameViewController.PlayingViewControl.CreateTargetItems(listTargetData);
                StartCoroutine(CRDecreasingTimeToCompleteLevel());
            }
        }


        /// <summary>
        /// Call Ingame_Revive event.
        /// </summary>
        public void Revive()
        {
            //Fire event
            IngameState = IngameState.Ingame_Revive;
            ingameState = IngameState.Ingame_Revive;

            //Add another actions here
            PauseBackgroundMusic(0.5f);
        }


        /// <summary>
        /// Call Ingame_GameOver event.
        /// </summary>
        public void GameOver()
        {
            //Fire event
            IngameState = IngameState.Ingame_GameOver;
            ingameState = IngameState.Ingame_GameOver;

            //Add another actions here
            StopBackgroundMusic(0f);
        }


        /// <summary>
        /// Call Ingame_CompletedLevel event.
        /// </summary>
        public void CompletedLevel()
        {
            //Fire event
            IngameState = IngameState.Ingame_CompletedLevel;
            ingameState = IngameState.Ingame_CompletedLevel;

            //Other actions

            StopBackgroundMusic(0f);
            ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.completedLevel);


            completedLevelEffectsTrans.gameObject.SetActive(true);
            foreach (ParticleSystem o in completedLevelEffects)
            {
                o.Play();
            }

            if (testingLevel == 0)
            {
                //Save level
                PlayerPrefs.SetInt(PlayerPrefsKey.SAVED_LEVEL_PPK, PlayerPrefs.GetInt(PlayerPrefsKey.SAVED_LEVEL_PPK) + 1);
            }
        }

        private void PlayBackgroundMusic(float delay)
        {
            StartCoroutine(CRPlayBGMusic(delay));
        }

        private IEnumerator CRPlayBGMusic(float delay)
        {
            yield return new WaitForSeconds(delay);
            ServicesManager.Instance.SoundManager.PlayMusic(background, 0.5f);
        }

        private void StopBackgroundMusic(float delay)
        {
            StartCoroutine(CRStopBGMusic(delay));
        }

        private IEnumerator CRStopBGMusic(float delay)
        {
            yield return new WaitForSeconds(delay);
            ServicesManager.Instance.SoundManager.StopMusic(0.5f);
        }

        private void PauseBackgroundMusic(float delay)
        {
            StartCoroutine(CRPauseBGMusic(delay));
        }

        private IEnumerator CRPauseBGMusic(float delay)
        {
            yield return new WaitForSeconds(delay);
            ServicesManager.Instance.SoundManager.PauseMusic();
        }

        private void ResumeBackgroundMusic(float delay)
        {
            StartCoroutine(CRResumeBGMusic(delay));
        }

        private IEnumerator CRResumeBGMusic(float delay)
        {
            yield return new WaitForSeconds(delay);
            ServicesManager.Instance.SoundManager.ResumeMusic();
        }



        /// <summary>
        /// Wait for an amount of time then create another merge cube.
        /// </summary>
        /// <returns></returns>
        private IEnumerator CRWaitAndCreateAnotherMergeCube()
        {
            yield return new WaitForSeconds(0.25f);
            if (ingameState == IngameState.Ingame_Playing)
                CreateMergeCubeForTheTable();
        }



        /// <summary>
        /// Decrease TimeToCompleteLevel.
        /// </summary>
        /// <returns></returns>
        private IEnumerator CRDecreasingTimeToCompleteLevel()
        {
            float totalTimeTemp = timeToCompleteLevel;
            float currentTimeTemp = timeToCompleteLevel;
            while (ingameState == IngameState.Ingame_Playing && currentTimeTemp > 0)
            {
                currentTimeTemp -= 0.05f;
                yield return new WaitForSeconds(0.05f);
                ViewManager.Instance.IngameViewController.PlayingViewControl.UpdateTimeProgressSlider(currentTimeTemp, totalTimeTemp);
            }

            if (ingameState == IngameState.Ingame_Playing)
            {
                HandleGameOver();
            }
        }



        /// <summary>
        /// Create a merge cube for the table. 
        /// </summary>
        private void CreateMergeCubeForTheTable()
        {
            //Calculate the total frequency
            float totalFreq = 0;
            foreach (IngameMergeCubeConfig o in currentLevelConfig.IngameMergeCubeConfigs)
            {
                totalFreq += o.Frequency;
            }

            float randomFrequency = UnityEngine.Random.Range(0, totalFreq);
            for (int i = 0; i < currentLevelConfig.IngameMergeCubeConfigs.Length; i++)
            {
                IngameMergeCubeConfig config = currentLevelConfig.IngameMergeCubeConfigs[i];
                if (randomFrequency < config.Frequency)
                {
                    //Create the merge cube
                    StartCoroutine(CRCreateMergeCube(config.IndexType));
                    break;
                }
                else
                {
                    randomFrequency -= config.Frequency;
                }
            }
        }


        /// <summary>
        /// Create a merge cube for the table.
        /// </summary>
        /// <param name="indexType"></param>
        /// <returns></returns>
        private IEnumerator CRCreateMergeCube(IndexType indexType)
        {
            MergeCubeController mergeCubeController = PoolManager.Instance.GetMergeCubeController(indexType);
            mergeCubeController.transform.position = new Vector3(0, 0, -6);
            mergeCubeController.transform.localEulerAngles = Vector3.zero;
            mergeCubeController.gameObject.SetActive(true);
            mergeCubeController.Rigidbody.isKinematic = true;
            mergeCubeController.ScaleUp();
            yield return new WaitForSeconds(0.25f);
            fadingLineTrans.transform.SetParent(mergeCubeController.transform);
            fadingLineTrans.transform.localPosition = new Vector3(0, -0.499f, -0.5f);
            fadingLineTrans.transform.localEulerAngles = new Vector3(90, 0, 0);
            fadingLineTrans.gameObject.SetActive(true);
            currentMergeCubeController = mergeCubeController;
        }


        //////////////////////////////////////Publish functions


        /// <summary>
        /// Set the environment type.
        /// </summary>
        /// <param name="type"></param>
        public static void SetEnvironmentType(EnvironmentType type)
        {
            EnvironmentType = type;
        }



        /// <summary>
        /// Continue the game
        /// </summary>
        public void SetContinueGame()
        {
            IsRevived = true;
            PlayingGame();
        }


        /// <summary>
        /// Handle action when game over.
        /// </summary>
        public void HandleGameOver()
        {
            if (IsRevived || !ServicesManager.Instance.AdManager.IsRewardedVideoAdReady())
            {
                GameOver();
            }
            else
            {
                Revive();
            }
        }

        /// <summary>
        /// Create a merge cube when 2 merge cubes collided.
        /// pos: the position that the merge cubes collide.
        /// indexType: the index type of that merge cube.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="indexType"></param>
        public void CreateMergeCubeInTheTable(Vector3 pos, IndexType indexType)
        {
            ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.mergeCubeCollided);
            pos = new Vector3((float)Math.Round(pos.x, 2), (float)Math.Round(pos.y, 2), (float)Math.Round(pos.z, 2));
            if (pos != savedMergeCubePos)
            {
                savedMergeCubePos = pos;
                MergeCubeController mergeCubeController = PoolManager.Instance.GetMergeCubeController(Utilities.GetNextIndexType(indexType));
                mergeCubeController.transform.position = savedMergeCubePos;
                mergeCubeController.gameObject.SetActive(true);
                mergeCubeController.ForcingUp();
                ViewManager.Instance.IngameViewController.PlayingViewControl.UpdateQuantity();
                EffectManager.Instance.CreateFadingCubeAndFade(mergeCubeController.transform);
            }
        }




        /// <summary>
        /// Find a object's transform that has the same IndexType of given MergeCubeController.
        /// </summary>
        /// <param name="givenMergeCubeController"></param>
        /// <returns></returns>
        public Transform FindSameIndexObject(MergeCubeController givenMergeCubeController)
        {
            Transform sameIndexTrans = null;
            MergeCubeController[] mergeCubeControllers = FindObjectsOfType<MergeCubeController>();
            foreach (MergeCubeController o in mergeCubeControllers)
            {
                if (o != givenMergeCubeController && o.IndexType.Equals(givenMergeCubeController.IndexType) && !o.Rigidbody.isKinematic)
                {
                    sameIndexTrans = o.transform;
                    break;
                }
            }

            return sameIndexTrans;
        }
    }
}
