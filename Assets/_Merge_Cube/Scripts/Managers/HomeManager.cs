﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CBGames
{
	public class HomeManager : MonoBehaviour
	{
		//[Header("Config")]

		[Header("References")]
        [SerializeField] private Transform fadingLineTrans = null;
        [SerializeField] private List<MergeCubeController> mergeCubeControllerPrefabs = new List<MergeCubeController>();
		[SerializeField] private Transform[] mergeCubePositions = null;
		[SerializeField] private EnvironmentConfig[] environmnetConfigs = null;

		private List<MergeCubeController> listMergeCubeController = new List<MergeCubeController>();
		private IEnumerator Start()
		{
			Application.targetFrameRate = 60;
			ViewManager.Instance.OnLoadingSceneDone(SceneManager.GetActiveScene().name);


            //Report level to leaderboard
            string username = PlayerPrefs.GetString(PlayerPrefsKey.SAVED_USER_NAME_PPK);
			if (!string.IsNullOrEmpty(username))
			{
				ServicesManager.Instance.LeaderboardManager.SetPlayerLeaderboardData();
			}


			//Setup table
			foreach(EnvironmentConfig o in environmnetConfigs)
            {
                if (o.EnvironmentType == IngameManager.EnvironmentType)
                {
					o.TableObject.SetActive(true);
                }
                else
                {
					o.TableObject.SetActive(false);
                }
            }


            //Create some merge cubes first
            fadingLineTrans.gameObject.SetActive(false);
            List<Transform> listInitialTrans = new List<Transform>();
            int number = Random.Range(3, 6);
            for(int i = 0; i < number; i++)
            {
                Transform initialTrans = mergeCubePositions[Random.Range(0, mergeCubePositions.Length)];
                while (listInitialTrans.Contains(initialTrans))
                {
                    initialTrans = mergeCubePositions[Random.Range(0, mergeCubePositions.Length)];
                }
                listInitialTrans.Add(initialTrans);
                MergeCubeController mergeCubeController = GetMergeCubeController();
                mergeCubeController.transform.position = initialTrans.position;
                mergeCubeController.transform.localEulerAngles = Vector3.zero;
                mergeCubeController.gameObject.SetActive(true);

                if (listInitialTrans.Count == number)
                    break;
            }

            yield return new WaitForSeconds(0.75f);

            //Create and move cube
            while (gameObject.activeInHierarchy)
            {
                //Create the merge cube
                MergeCubeController mergeCubeController = GetMergeCubeController();
                mergeCubeController.transform.position = new Vector3(0, 0, -6);
                mergeCubeController.transform.localEulerAngles = Vector3.zero;
                mergeCubeController.gameObject.SetActive(true);
                mergeCubeController.Rigidbody.isKinematic = true;
                mergeCubeController.ScaleUp();
                yield return new WaitForSeconds(0.25f);
                fadingLineTrans.transform.SetParent(mergeCubeController.transform);
                fadingLineTrans.transform.localPosition = new Vector3(0, -0.499f, -0.5f);
                fadingLineTrans.transform.localEulerAngles = new Vector3(90, 0, 0);
                fadingLineTrans.gameObject.SetActive(true);

                //Find the location of the same merge cube in the table
                float endX = mergeCubeController.transform.position.x;
                MergeCubeController[] mergeCubeControllers = FindObjectsOfType<MergeCubeController>();
                foreach(MergeCubeController o in mergeCubeControllers)
                {
                    if (o.IndexType == mergeCubeController.IndexType && !o.Rigidbody.isKinematic)
                    {
                        endX = o.transform.position.x;
                        break;
                    }
                }

                //Move the current merge cube to the x location
                float movingTime = 0.35f;
                Vector3 startPos = mergeCubeController.transform.position;
                Vector3 endPos = new Vector3(endX, startPos.y, startPos.z);
                float t = 0;
                while (t < movingTime)
                {
                    t += Time.deltaTime;
                    float factor = t / movingTime;
                    mergeCubeController.transform.position = Vector3.Lerp(startPos, endPos, factor);
                    yield return null;
                }

                yield return new WaitForSeconds(0.25f);
                fadingLineTrans.SetParent(null);
                fadingLineTrans.gameObject.SetActive(false);
                mergeCubeController.Rigidbody.isKinematic = false;
                mergeCubeController.Rigidbody.velocity = Vector3.forward * 25f;
                yield return new WaitForSeconds(1f);
            }
        }





        /// <summary>
        /// Get a random IndexType of INDEX_2, INDEX_4 or INDEX_8.
        /// </summary>
        /// <returns></returns>
        private IndexType GetRandomIndexType()
        {
            int value = Random.Range(0, 8);
            if (value == 0)
                return IndexType.INDEX_2;
            if (value == 1)
                return IndexType.INDEX_4;
            if (value == 2)
                return IndexType.INDEX_8;
            if (value == 3)
                return IndexType.INDEX_16;
            if (value == 4)
                return IndexType.INDEX_32;
            if (value == 5)
                return IndexType.INDEX_64;
            if (value == 6)
                return IndexType.INDEX_128;
            else
                return IndexType.INDEX_256;
        }



        /// <summary>
        /// Get an inactive MergeCubeController object.
        /// </summary>
        /// <returns></returns>
        private MergeCubeController GetMergeCubeController()
        {
            IndexType randomIndexType = GetRandomIndexType();

            //Find in the list
            MergeCubeController mergeCubeController = listMergeCubeController.Where(a => !a.gameObject.activeInHierarchy && a.IndexType.Equals(randomIndexType)).FirstOrDefault();

            if (mergeCubeController == null)
            {
                //Did not find one -> create new one
                MergeCubeController prefab = mergeCubeControllerPrefabs.Where(a => a.IndexType.Equals(randomIndexType)).FirstOrDefault();
                mergeCubeController = Instantiate(prefab, Vector3.zero, Quaternion.identity);
                mergeCubeController.gameObject.SetActive(false);
                listMergeCubeController.Add(mergeCubeController);
            }

            return mergeCubeController;
        }
    }
}

