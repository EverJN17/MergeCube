﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CBGames
{
    public class IndexController : MonoBehaviour
    {
        [SerializeField] private IndexType indexType = IndexType.INDEX_2;
        public IndexType IndexType { get { return indexType; } }
    }
}
