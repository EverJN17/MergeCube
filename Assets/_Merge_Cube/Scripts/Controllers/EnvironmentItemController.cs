﻿using UnityEngine;
using UnityEngine.UI;

namespace CBGames
{
    public class EnvironmentItemController : MonoBehaviour
    {

        [Header("Config")]
        [SerializeField] private EnvironmentType environmentType = EnvironmentType.GREEN;

        [Header("References")]
        [SerializeField] private Text environmentTypeTxt = null;

        public void OnSetup()
        {
            environmentTypeTxt.text = environmentType.ToString().Replace('_', ' ');
        }

        public void SelectBtn()
        {
            IngameManager.SetEnvironmentType(environmentType);
            ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.button);
            ViewManager.Instance.LoadScene("Ingame", 0.2f);
        }
    }
}
