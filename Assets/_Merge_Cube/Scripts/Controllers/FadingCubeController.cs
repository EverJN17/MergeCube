﻿using System.Collections;
using UnityEngine;

namespace CBGames
{
    public class FadingCubeController : MonoBehaviour
    {
        [SerializeField] private MeshRenderer meshRenderer = null;


        public void StartFade()
        {
            transform.localPosition = Vector3.zero;
            transform.localEulerAngles = Vector3.zero;
            transform.localScale = Vector3.one;
            StartCoroutine(CRStartFading());
        }
        private IEnumerator CRStartFading()
        {
            float fadingTime = 0.25f;
            Vector3 startScale = transform.localScale;
            Vector3 endScale = startScale * 3f;
            Color startColor = meshRenderer.material.color;
            Color endColor = new Color(startColor.r, startColor.g, startColor.b, 0);
            float t = 0;
            while (t < fadingTime)
            {
                t += Time.deltaTime;
                float factor = t / fadingTime;
                transform.localScale = Vector3.Lerp(startScale, endScale, factor);
                meshRenderer.material.color = Color.Lerp(startColor, endColor, factor);
                yield return null;
            }
            meshRenderer.material.color = startColor;
            transform.SetParent(null);
            gameObject.SetActive(false);
        }
    }

}