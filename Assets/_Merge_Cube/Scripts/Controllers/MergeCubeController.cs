﻿using System.Collections;
using UnityEngine;

namespace CBGames
{
    public class MergeCubeController : MonoBehaviour
    {
        [SerializeField] private IndexType indexType = IndexType.INDEX_2;
        [SerializeField] private MeshRenderer meshRenderer = null;
        [SerializeField] private Rigidbody rigid = null;

        public IndexType IndexType { get { return indexType; } }
        public Rigidbody Rigidbody { get { return rigid; } }

        private int hitFinishWallCount = 0;
        private void OnCollisionEnter(Collision collision)
        {
            if (IngameManager.Instance != null) //In Ingame scene
            {
                if (IngameManager.Instance.IngameState == IngameState.Ingame_Playing)
                {
                    if (collision.collider.CompareTag("Player"))
                    {
                        MergeCubeController other = collision.collider.GetComponent<MergeCubeController>();
                        if (other.IndexType.Equals(indexType) && indexType != IngameManager.Instance.HighestTargetIndexType)
                        {
                            gameObject.SetActive(false);
                            hitFinishWallCount = 0;
                            IngameManager.Instance.CreateMergeCubeInTheTable(collision.contacts[0].point, indexType);
                        }
                    }
                }
            }
            else
            {
                if (collision.collider.CompareTag("Player"))
                {
                    MergeCubeController other = collision.collider.GetComponent<MergeCubeController>();
                    if (other.IndexType.Equals(indexType))
                    {
                        gameObject.SetActive(false);
                    }
                }
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (IngameManager.Instance.IngameState == IngameState.Ingame_Playing)
            {
                if (other.CompareTag("Finish"))
                {
                    hitFinishWallCount++;
                    if (hitFinishWallCount >= 2)
                    {
                        ServicesManager.Instance.SoundManager.PlayOneSound(ServicesManager.Instance.SoundManager.mergeCubeExploded);
                        EffectManager.Instance.PlayMergeCubeExplodeEffect(transform.position);
                        gameObject.SetActive(false);
                        hitFinishWallCount = 0;
                        IngameManager.Instance.HandleGameOver();
                    }
                }
            }
        }



        /// <summary>
        /// Scale this merge cube up.
        /// </summary>
        public void ScaleUp()
        {
            StartCoroutine(CRScalingUp());
        }
        private IEnumerator CRScalingUp()
        {
            float scalingTime = 0.25f;
            float t = 0;
            while (t < scalingTime)
            {
                t += Time.deltaTime;
                float factor = t / scalingTime;
                transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, factor);
                yield return null;
            }
        }



        /// <summary>
        /// This is the merge cube created when 2 merge cubes collided.
        /// </summary>
        public void ForcingUp()
        {
            hitFinishWallCount++;
            rigid.velocity = Vector3.zero;
            Transform sameIndexTrans = IngameManager.Instance.FindSameIndexObject(this);
            if (sameIndexTrans == null)
            {
                rigid.AddForce((Vector3.up + Vector3.forward * 0.1f) * 1000f);
                rigid.AddTorque(Vector3.right * 50f);
            }
            else
            {
                Vector3 startPos = sameIndexTrans.position;
                Vector3 endPos = new Vector3(transform.position.x, startPos.y, transform.position.z);
                Vector3 flyingDir = (startPos - endPos).normalized;
                rigid.AddForce((Vector3.up + flyingDir * 0.15f) * 1000f);
                rigid.AddTorque((Vector3.up + flyingDir * 0.2f) * 50f);
            }
        }



        /// <summary>
        /// Setup color for this merge cube.
        /// </summary>
        /// <param name="color"></param>
        public void SetUpColor(Color color)
        {
            meshRenderer.sharedMaterial.color = color;
        }
    }
}
