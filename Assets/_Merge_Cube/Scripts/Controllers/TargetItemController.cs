﻿using UnityEngine;
using UnityEngine.UI;

namespace CBGames
{
    public class TargetItemController : MonoBehaviour
    {
        [SerializeField] private Image targetImg = null;
        [SerializeField] private Text quantityProgressTxt = null;

        public IndexType IndexType { private set; get; }

        private int endQuantity = 0;
        private int currentQuantity = 0;
        public void OnSetup(Sprite targetSprite, IndexType indexType, int targetQuantity)
        {
            targetImg.overrideSprite = targetSprite;
            IndexType = indexType;
            endQuantity = targetQuantity;
            quantityProgressTxt.text = "0/" + endQuantity.ToString();
        }


        /// <summary>
        /// Update the current quantity
        /// </summary>
        /// <param name="quantity"></param>
        public void UpdateQuantity(int quantity)
        {
            currentQuantity = quantity;
            quantityProgressTxt.text = currentQuantity.ToString() + "/" + endQuantity.ToString();
        }


        /// <summary>
        /// Checking wheather this target item reached it's goal. 
        /// </summary>
        /// <returns></returns>
        public bool IsReachedGoal()
        {
            return currentQuantity == endQuantity;
        }
    }
}
