﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace CBGames
{
    public class SceneLoader : MonoBehaviour
    {

        private static string targetScene = string.Empty;

        private void Start()
        {
            ViewManager.Instance.OnLoadingSceneDone(SceneManager.GetActiveScene().name);
            StartCoroutine(LoadingScene());
        }

        private IEnumerator LoadingScene()
        {
            int count = 0;
            float timeTemp = 0.5f;
            float t = 0;
            while (t < timeTemp)
            {
                t += Time.deltaTime;
                float factor = t / timeTemp;
                ViewManager.Instance.LoadingViewController.SetLoadingAmount(Mathf.Lerp(0f, 0.5f, factor));
                yield return null;

                count++;
                if (count == 1)
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING.");
                else if (count == 2)
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING..");
                else
                {
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING...");
                    count = 0;
                }
            }

            AsyncOperation asyn = SceneManager.LoadSceneAsync(targetScene);
            while (!asyn.isDone)
            {
                count++;
                if (count == 1)
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING.");
                else if (count == 2)
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING..");
                else
                {
                    ViewManager.Instance.LoadingViewController.SetLoadingText("LOADING...");
                    count = 0;
                }
                yield return null;
                if (asyn.progress >= 0.5f)
                    ViewManager.Instance.LoadingViewController.SetLoadingAmount(asyn.progress);
            }
        }

        /// <summary>
        /// Set target scene.
        /// </summary>
        /// <param name="sceneName"></param>
        public static void SetTargetScene(string sceneName)
        {
            targetScene = sceneName;
        }
    }
}
