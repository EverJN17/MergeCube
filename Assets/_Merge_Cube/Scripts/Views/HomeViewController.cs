﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace CBGames
{
    public class HomeViewController : MonoBehaviour
    {

        [SerializeField] private RectTransform topBarTrans = null;
        [SerializeField] private RectTransform gameNameTrans = null;
        [SerializeField] private RectTransform playButtonTrans = null;
        [SerializeField] private RectTransform settingBtnTrans = null;
        [SerializeField] private RectTransform rateAppBtnTrans = null;
        [SerializeField] private RectTransform nativeShareBtnTrans = null;
       // [SerializeField] private RectTransform removeAdsButtonTrans = null;
        [SerializeField] private RectTransform soundButtonsTrans = null;
        [SerializeField] private RectTransform musicButtonsTrans = null;
        //[SerializeField] private RectTransform leaderboardButtonTrans = null;
      //  [SerializeField] private RectTransform facebookShareBtnTrans = null;
       // [SerializeField] private RectTransform twitterShareBtnTrans = null;
        [SerializeField] private GameObject soundOnBtn = null;
        [SerializeField] private GameObject soundOffBtn = null;
        [SerializeField] private GameObject musicOnBtn = null;
        [SerializeField] private GameObject musicOffBtn = null;
        [SerializeField] private Text currentLevelTxt = null;
        [SerializeField] private LeaderboardViewController leaderboardViewController = null;
        [SerializeField] private EnvironmentViewController environmentViewController = null;


        private int settingButtonTurn = 1;
        public void OnShow()
        {
            ViewManager.Instance.MoveRect(topBarTrans, topBarTrans.anchoredPosition, new Vector2(topBarTrans.anchoredPosition.x, 0), 0.5f);
            ViewManager.Instance.ScaleRect(gameNameTrans, Vector2.zero, Vector2.one, 1f);
            StartCoroutine(CRShowingBottomButtons());

            leaderboardViewController.gameObject.SetActive(false);
            environmentViewController.gameObject.SetActive(false);
            settingButtonTurn = 1;
            currentLevelTxt.text = "LEVEL: " + PlayerPrefs.GetInt(PlayerPrefsKey.SAVED_LEVEL_PPK, 1).ToString();

            //Update sound btns
            if (ServicesManager.Instance.SoundManager.IsSoundOff())
            {
                soundOnBtn.gameObject.SetActive(false);
                soundOffBtn.gameObject.SetActive(true);
            }
            else
            {
                soundOnBtn.gameObject.SetActive(true);
                soundOffBtn.gameObject.SetActive(false);
            }

            //Update music btns
            if (ServicesManager.Instance.SoundManager.IsMusicOff())
            {
                musicOffBtn.gameObject.SetActive(true);
                musicOnBtn.gameObject.SetActive(false);
            }
            else
            {
                musicOffBtn.gameObject.SetActive(false);
                musicOnBtn.gameObject.SetActive(true);
            }
        }


        private void OnDisable()
        {
            topBarTrans.anchoredPosition = new Vector2(topBarTrans.anchoredPosition.x, 100);
            gameNameTrans.localScale = Vector2.zero;

            playButtonTrans.localScale = Vector3.zero;
            settingBtnTrans.anchoredPosition = new Vector2(settingBtnTrans.anchoredPosition.x, -200);
            rateAppBtnTrans.anchoredPosition = new Vector2(rateAppBtnTrans.anchoredPosition.x, -200);
            nativeShareBtnTrans.anchoredPosition = new Vector2(nativeShareBtnTrans.anchoredPosition.x, -200);

            //removeAdsButtonTrans.anchoredPosition = new Vector2(-150, removeAdsButtonTrans.anchoredPosition.y);
            soundButtonsTrans.anchoredPosition = new Vector2(-150, soundButtonsTrans.anchoredPosition.y);
            musicButtonsTrans.anchoredPosition = new Vector2(-150, musicButtonsTrans.anchoredPosition.y);
            //leaderboardButtonTrans.anchoredPosition = new Vector2(150, leaderboardButtonTrans.anchoredPosition.y);
            //facebookShareBtnTrans.anchoredPosition = new Vector2(150, facebookShareBtnTrans.anchoredPosition.y);
            //twitterShareBtnTrans.anchoredPosition = new Vector2(150, twitterShareBtnTrans.anchoredPosition.y);
        }



        /// <summary>
        /// Handle the Home view when Dailyreward view or Environment view closes.
        /// </summary>
        public void OnSubViewClose()
        {
            ViewManager.Instance.ScaleRect(gameNameTrans, Vector2.zero, Vector2.one, 1f);
            StartCoroutine(CRShowingBottomButtons());
        }


        private IEnumerator CRShowingBottomButtons()
        {
            ViewManager.Instance.ScaleRect(playButtonTrans, Vector2.zero, Vector2.one, 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(nativeShareBtnTrans, nativeShareBtnTrans.anchoredPosition, new Vector2(nativeShareBtnTrans.anchoredPosition.x, 150), 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(rateAppBtnTrans, rateAppBtnTrans.anchoredPosition, new Vector2(rateAppBtnTrans.anchoredPosition.x, 150), 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(settingBtnTrans, settingBtnTrans.anchoredPosition, new Vector2(settingBtnTrans.anchoredPosition.x, 150), 0.5f);
        }


        //////////////////////////////////////////////////////////////////////UI Functions


        public void PlayBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            StartCoroutine(CRHandlePlayBtn());
        }
        private IEnumerator CRHandlePlayBtn()
        {
            if (settingButtonTurn == -1)
                SettingBtn();
            ViewManager.Instance.ScaleRect(gameNameTrans, Vector2.one, Vector2.zero, 1f);
            ViewManager.Instance.ScaleRect(playButtonTrans, Vector2.one, Vector2.zero, 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(settingBtnTrans, settingBtnTrans.anchoredPosition, new Vector2(settingBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);
            ViewManager.Instance.MoveRect(rateAppBtnTrans, rateAppBtnTrans.anchoredPosition, new Vector2(rateAppBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);
            ViewManager.Instance.MoveRect(nativeShareBtnTrans, nativeShareBtnTrans.anchoredPosition, new Vector2(nativeShareBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);

            environmentViewController.gameObject.SetActive(true);
            environmentViewController.OnShow();
        }



        public void NativeShareBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ServicesManager.Instance.ShareManager.NativeShare();
        }

        public void SettingBtn()
        {
            settingButtonTurn *= -1;
            StartCoroutine(CRHandleSettingBtn());
        }
        private IEnumerator CRHandleSettingBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            if (settingButtonTurn == -1)
            {
                //ViewManager.Instance.MoveRect(removeAdsButtonTrans, removeAdsButtonTrans.anchoredPosition, new Vector2(0, removeAdsButtonTrans.anchoredPosition.y), 0.5f);
               // ViewManager.Instance.MoveRect(leaderboardButtonTrans, leaderboardButtonTrans.anchoredPosition, new Vector2(0, leaderboardButtonTrans.anchoredPosition.y), 0.5f);

                yield return new WaitForSeconds(0.08f);

                ViewManager.Instance.MoveRect(soundButtonsTrans, soundButtonsTrans.anchoredPosition, new Vector2(0, soundButtonsTrans.anchoredPosition.y), 0.5f);
              //  ViewManager.Instance.MoveRect(facebookShareBtnTrans, facebookShareBtnTrans.anchoredPosition, new Vector2(0, facebookShareBtnTrans.anchoredPosition.y), 0.5f);

                yield return new WaitForSeconds(0.08f);

                ViewManager.Instance.MoveRect(musicButtonsTrans, musicButtonsTrans.anchoredPosition, new Vector2(0, musicButtonsTrans.anchoredPosition.y), 0.5f);
              //  ViewManager.Instance.MoveRect(twitterShareBtnTrans, twitterShareBtnTrans.anchoredPosition, new Vector2(0, twitterShareBtnTrans.anchoredPosition.y), 0.5f);
            }
            else
            {
               // ViewManager.Instance.MoveRect(removeAdsButtonTrans, removeAdsButtonTrans.anchoredPosition, new Vector2(-150, removeAdsButtonTrans.anchoredPosition.y), 0.5f);
               // ViewManager.Instance.MoveRect(leaderboardButtonTrans, leaderboardButtonTrans.anchoredPosition, new Vector2(150, leaderboardButtonTrans.anchoredPosition.y), 0.5f);

                yield return new WaitForSeconds(0.08f);

                ViewManager.Instance.MoveRect(soundButtonsTrans, soundButtonsTrans.anchoredPosition, new Vector2(-150, soundButtonsTrans.anchoredPosition.y), 0.5f);
               // ViewManager.Instance.MoveRect(facebookShareBtnTrans, facebookShareBtnTrans.anchoredPosition, new Vector2(150, facebookShareBtnTrans.anchoredPosition.y), 0.5f);

                yield return new WaitForSeconds(0.08f);

                ViewManager.Instance.MoveRect(musicButtonsTrans, musicButtonsTrans.anchoredPosition, new Vector2(-150, musicButtonsTrans.anchoredPosition.y), 0.5f);
               // ViewManager.Instance.MoveRect(twitterShareBtnTrans, twitterShareBtnTrans.anchoredPosition, new Vector2(150, twitterShareBtnTrans.anchoredPosition.y), 0.5f);
            }
        }


        public void LeaderboardBtn()
        {
            SettingBtn();
            StartCoroutine(CRHanleLeaderboardBtn());
        }
        private IEnumerator CRHanleLeaderboardBtn()
        {
            ViewManager.Instance.ScaleRect(gameNameTrans, Vector2.one, Vector2.zero, 1f);
            ViewManager.Instance.ScaleRect(playButtonTrans, Vector2.one, Vector2.zero, 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(settingBtnTrans, settingBtnTrans.anchoredPosition, new Vector2(settingBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);
            ViewManager.Instance.MoveRect(rateAppBtnTrans, rateAppBtnTrans.anchoredPosition, new Vector2(rateAppBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);
            ViewManager.Instance.MoveRect(nativeShareBtnTrans, nativeShareBtnTrans.anchoredPosition, new Vector2(nativeShareBtnTrans.anchoredPosition.x, -200f), 0.5f);
            yield return new WaitForSeconds(0.08f);

            leaderboardViewController.gameObject.SetActive(true);
            leaderboardViewController.OnShow();
        }


        public void ToggleSound()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ServicesManager.Instance.SoundManager.ToggleSound();
            if (ServicesManager.Instance.SoundManager.IsSoundOff())
            {
                soundOnBtn.gameObject.SetActive(false);
                soundOffBtn.gameObject.SetActive(true);
            }
            else
            {
                soundOnBtn.gameObject.SetActive(true);
                soundOffBtn.gameObject.SetActive(false);
            }
        }

        public void ToggleMusic()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ServicesManager.Instance.SoundManager.ToggleMusic();
            if (ServicesManager.Instance.SoundManager.IsMusicOff())
            {
                musicOffBtn.gameObject.SetActive(true);
                musicOnBtn.gameObject.SetActive(false);
            }
            else
            {
                musicOffBtn.gameObject.SetActive(false);
                musicOnBtn.gameObject.SetActive(true);
            }
        }

        public void FacebookShareBtn()
        {
          //  ViewManager.Instance.PlayClickButtonSound();
           // ServicesManager.Instance.ShareManager.FacebookShare();
        }
        public void TwitterShareBtn()
        {
            //ViewManager.Instance.PlayClickButtonSound();
           // ServicesManager.Instance.ShareManager.TwitterShare();
        }
        public void RateAppBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            Application.OpenURL(ServicesManager.Instance.ShareManager.AppUrl);
        }
    }
}
