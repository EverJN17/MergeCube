﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CBGames
{
    public class EndGameViewController : MonoBehaviour
    {

        [SerializeField] private RectTransform topBarTrans = null;
        [SerializeField] private Text nextLevelTxt = null;
        [SerializeField] private Text levelResultTxt = null;
        [SerializeField] private RectTransform replayBtnTrans = null;
        [SerializeField] private RectTransform nextLevelBtnTrans = null;
        [SerializeField] private RectTransform shareBtnTrans = null;
        [SerializeField] private RectTransform characterBtnTrans = null;
        [SerializeField] private RectTransform homeBtnTrans = null;

        private RectTransform mainBtnTrans = null;
        public void OnShow()
        {
            if (IngameManager.Instance.IngameState == IngameState.Ingame_CompletedLevel)
            {
                levelResultTxt.text = "LEVEL COMPLETED !";
                levelResultTxt.color = Color.green;
                replayBtnTrans.gameObject.SetActive(false);
                nextLevelBtnTrans.gameObject.SetActive(true);
                mainBtnTrans = nextLevelBtnTrans;
            }
            else if (IngameManager.Instance.IngameState == IngameState.Ingame_GameOver)
            {
                levelResultTxt.text = "LEVEL FAILED !";
                levelResultTxt.color = Color.red;
                replayBtnTrans.gameObject.SetActive(true);
                nextLevelBtnTrans.gameObject.SetActive(false);
                mainBtnTrans = replayBtnTrans;
            }

            ViewManager.Instance.MoveRect(topBarTrans, topBarTrans.anchoredPosition, new Vector2(topBarTrans.anchoredPosition.x, 0), 0.5f);
            ViewManager.Instance.ScaleRect(levelResultTxt.rectTransform, Vector2.zero, Vector2.one, 0.75f);
            StartCoroutine(CRShowBottomBtns());
            nextLevelTxt.text = "NEXT LEVEL: " + PlayerPrefs.GetInt(PlayerPrefsKey.SAVED_LEVEL_PPK);
        }

        private void OnDisable()
        {
            topBarTrans.anchoredPosition = new Vector2(topBarTrans.anchoredPosition.x, 200);
            levelResultTxt.rectTransform.localScale = Vector2.zero;

            replayBtnTrans.localScale = Vector3.zero;
            nextLevelBtnTrans.localScale = Vector3.zero;
            shareBtnTrans.anchoredPosition = new Vector2(shareBtnTrans.anchoredPosition.x, -200);
            characterBtnTrans.anchoredPosition = new Vector2(characterBtnTrans.anchoredPosition.x, -200);
            homeBtnTrans.anchoredPosition = new Vector2(homeBtnTrans.anchoredPosition.x, -200);
        }


        private IEnumerator CRShowBottomBtns()
        {
            ViewManager.Instance.ScaleRect(mainBtnTrans, Vector2.zero, Vector2.one, 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(shareBtnTrans, shareBtnTrans.anchoredPosition, new Vector2(shareBtnTrans.anchoredPosition.x, 150), 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(characterBtnTrans, characterBtnTrans.anchoredPosition, new Vector2(characterBtnTrans.anchoredPosition.x, 150), 0.5f);
            yield return new WaitForSeconds(0.15f);
            ViewManager.Instance.MoveRect(homeBtnTrans, homeBtnTrans.anchoredPosition, new Vector2(homeBtnTrans.anchoredPosition.x, 150), 0.5f);
        }

        public void PlayBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ViewManager.Instance.LoadScene("Ingame", 0.3f);
        }

        public void ShareBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ServicesManager.Instance.ShareManager.NativeShare();
        }

        public void RateAppBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            Application.OpenURL(ServicesManager.Instance.ShareManager.AppUrl);
        }

        public void HomeBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ViewManager.Instance.LoadScene("Home", 0.3f);
        }
    }
}
