﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace CBGames
{
    public class ReviveViewController : MonoBehaviour
    {

        [SerializeField] private RectTransform tryAgainImageTrans = null;
        [SerializeField] private RectTransform sunbrustImageTrans = null;
        [SerializeField] private RectTransform reviveBtnTrans = null;
        [SerializeField] private RectTransform closeReviveViewBtnTrans = null;
        [SerializeField] private Text countDownTxt = null;


        public void OnShow()
        {
            ViewManager.Instance.ScaleRect(tryAgainImageTrans, Vector2.zero, Vector2.one, 0.5f);
            ViewManager.Instance.ScaleRect(sunbrustImageTrans, Vector2.zero, Vector2.one, 0.5f);
            countDownTxt.gameObject.SetActive(false);
            StartCoroutine(CROnShow());
        }

        private void OnDisable()
        {
            tryAgainImageTrans.localScale = Vector2.zero;
            sunbrustImageTrans.localScale = Vector2.zero;
            closeReviveViewBtnTrans.localScale = Vector2.zero;
        }



        private IEnumerator CROnShow()
        {
            yield return new WaitForSeconds(0.5f);
            ViewManager.Instance.ScaleRect(reviveBtnTrans, Vector2.zero, Vector2.one, 0.5f);
            countDownTxt.gameObject.SetActive(true);
            StartCoroutine(CRReviveCountDown());
            StartCoroutine(CRRotatingSunbrustImage());
            yield return new WaitForSeconds(1f);
            ViewManager.Instance.ScaleRect(closeReviveViewBtnTrans, Vector2.zero, Vector2.one, 0.5f);
        }



        /// <summary>
        /// Start counting down revive wait time.
        /// </summary>
        /// <returns></returns>
        private IEnumerator CRReviveCountDown()
        {
            float t = IngameManager.Instance.ReviveWaitTime;
            float decreaseAmount = 1f;
            while (t > 0)
            {
                if (IngameManager.Instance.IngameState != IngameState.Ingame_Revive)
                    yield break;
                t -= decreaseAmount;
                countDownTxt.text = t.ToString();

                float scaleTime = decreaseAmount / 2f;
                float countTime = 0;
                while (countTime < scaleTime)
                {
                    countTime += Time.deltaTime;
                    float factor = countTime / scaleTime;
                    countDownTxt.rectTransform.localScale = Vector2.Lerp(Vector2.one, Vector2.one * 1.25f, factor);
                    yield return null;
                }
                countTime = 0;
                while (countTime < scaleTime)
                {
                    countTime += Time.deltaTime;
                    float factor = countTime / scaleTime;
                    countDownTxt.rectTransform.localScale = Vector2.Lerp(Vector2.one * 1.25f, Vector2.one, factor);
                    yield return null;
                }
            }
            IngameManager.Instance.GameOver();
        }





        /// <summary>
        /// Rotate the sunbrust image. 
        /// </summary>
        /// <returns></returns>
        private IEnumerator CRRotatingSunbrustImage()
        {
            while (gameObject.activeInHierarchy)
            {
                sunbrustImageTrans.localEulerAngles += Vector3.forward * 100f * Time.deltaTime;
                yield return null;
            }
        }


        public void ReviveBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ServicesManager.Instance.AdManager.ShowRewardedVideoAd();
        }

        public void CloseReviveViewBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            IngameManager.Instance.GameOver();
        }
    }
}
