﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CBGames
{
    public class PlayingViewController : MonoBehaviour
    {

        [SerializeField] private RectTransform topBarTrans = null;
        [SerializeField] private RectTransform targetItemBarTrans = null;
        [SerializeField] private Text currentLevelTxt = null;
        [SerializeField] private Image timeProgressSlider = null;
        [SerializeField] private TargetItemController targetItemControllerPrefab = null;
        [SerializeField] private TargetSpriteConfig[] targetSpriteConfigs = null;

        private Dictionary<IndexType, TargetItemController> targetItemControllers = new Dictionary<IndexType, TargetItemController>();
        private List<TargetItemController> listTargetItemControllerIngame = new List<TargetItemController>();
        public void OnShow()
        {
            ViewManager.Instance.MoveRect(topBarTrans, topBarTrans.anchoredPosition, new Vector2(topBarTrans.anchoredPosition.x, 0), 0.5f);
            ViewManager.Instance.MoveRect(targetItemBarTrans, targetItemBarTrans.anchoredPosition, new Vector2(5, targetItemBarTrans.anchoredPosition.y), 0.5f);
            currentLevelTxt.text = "LEVEL: " + IngameManager.Instance.CurrentLevel.ToString();
        }

        private void OnDisable()
        {
            topBarTrans.anchoredPosition = new Vector2(topBarTrans.anchoredPosition.x, 100);
            targetItemBarTrans.anchoredPosition = new Vector2(-110, targetItemBarTrans.anchoredPosition.y);

            if (IngameManager.Instance.IngameState == IngameState.Ingame_GameOver || IngameManager.Instance.IngameState== IngameState.Ingame_CompletedLevel)
            {
                foreach(TargetItemController o in listTargetItemControllerIngame)
                {
                    o.gameObject.SetActive(false);
                }
                listTargetItemControllerIngame.Clear();
            }
        }




        /// <summary>
        /// Create target items for the view.
        /// </summary>
        /// <param name="targetDatas"></param>
        public void CreateTargetItems(List<TargetData> targetDatas)
        {
            for (int i = 0; i < targetDatas.Count; i++)
            {
                TargetItemController targetItemController = GetTargetItemController(targetDatas[i].IndexType);
                targetItemController.gameObject.SetActive(true);
                targetItemController.transform.SetParent(targetItemBarTrans);
                targetItemController.transform.localScale = Vector3.one;
                targetItemController.OnSetup(GetTargetSprite(targetDatas[i].IndexType), targetDatas[i].IndexType, targetDatas[i].Quantity);
                listTargetItemControllerIngame.Add(targetItemController);
            }
        }



        /// <summary>
        /// Update the quantity number for the TargetItemController that matched the indexType
        /// </summary>
        /// <param name="indexType"></param>
        public void UpdateQuantity()
        {
            StartCoroutine(CRWaitandUpdateQuantity());
        }

        private IEnumerator CRWaitandUpdateQuantity()
        {
            yield return new WaitForSeconds(0.25f);
            MergeCubeController[] mergeCubeControllers = FindObjectsOfType<MergeCubeController>();
            foreach (TargetItemController o in listTargetItemControllerIngame)
            {
                int quantityCount = 0;
                foreach (MergeCubeController a in mergeCubeControllers)
                {
                    if (!a.Rigidbody.isKinematic && a.IndexType == o.IndexType)
                    {
                        quantityCount++;
                    }
                }
                o.UpdateQuantity(quantityCount);
            }

            yield return null;
            foreach (TargetItemController o in listTargetItemControllerIngame)
            {
                if (!o.IsReachedGoal())
                {
                    yield break;
                }
            }

            //All the target items reached it's goal -> complete level.
            IngameManager.Instance.CompletedLevel();
        }



        /// <summary>
        /// Update the timeProgressSlider.
        /// </summary>
        /// <param name="currentTime"></param>
        /// <param name="totalTime"></param>
        public void UpdateTimeProgressSlider(float currentTime, float totalTime)
        {
            timeProgressSlider.fillAmount = currentTime / totalTime;
        }





        private Sprite GetTargetSprite(IndexType indexType)
        {
            foreach(TargetSpriteConfig o in targetSpriteConfigs)
            {
                if (o.IndexType == indexType)
                {
                    return o.TargetSprite;
                }
            }
            return null;
        }

        private TargetItemController GetTargetItemController(IndexType indexType)
        {
            TargetItemController targetItemController = null;
            if (targetItemControllers.ContainsKey(indexType))
            {
                targetItemControllers.TryGetValue(indexType, out targetItemController);
            }
            else
            {
                targetItemController = Instantiate(targetItemControllerPrefab, Vector3.zero, Quaternion.identity);
                targetItemControllers.Add(indexType, targetItemController);
                targetItemController.transform.SetParent(targetItemBarTrans);

            }
            return targetItemController;
        }
    }
}
