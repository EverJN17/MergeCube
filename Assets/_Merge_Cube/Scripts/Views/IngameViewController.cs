﻿using UnityEngine;
using System.Collections;

namespace CBGames
{
    public class IngameViewController : MonoBehaviour
    {

        [SerializeField] private PlayingViewController playingViewControl = null;
        [SerializeField] private ReviveViewController reviveViewControl = null;
        [SerializeField] private EndGameViewController endGameViewControl = null;

        public PlayingViewController PlayingViewControl { get { return playingViewControl; } }
        public void OnShow()
        {
            IngameManager.GameStateChanged += GameManager_GameStateChanged;
        }

        private void OnDisable()
        {
            IngameManager.GameStateChanged -= GameManager_GameStateChanged;
        }

        private void GameManager_GameStateChanged(IngameState obj)
        {
            if (obj == IngameState.Ingame_Revive)
            {
                StartCoroutine(CRShowReviveView());
            }
            else if (obj == IngameState.Ingame_CompletedLevel)
            {
                StartCoroutine(CRShowEndGameView(1.15f));
            }
            else if (obj == IngameState.Ingame_GameOver)
            {
                StartCoroutine(CRShowEndGameView(0f));
            }
            else if (obj == IngameState.Ingame_Playing)
            {
                playingViewControl.gameObject.SetActive(true);
                playingViewControl.OnShow();

                reviveViewControl.gameObject.SetActive(false);
                endGameViewControl.gameObject.SetActive(false);
            }
        }

        private IEnumerator CRShowEndGameView(float delay)
        {
            yield return new WaitForSeconds(delay);
            endGameViewControl.gameObject.SetActive(true);
            endGameViewControl.OnShow();

            reviveViewControl.gameObject.SetActive(false);
            playingViewControl.gameObject.SetActive(false);
        }


        private IEnumerator CRShowReviveView()
        {
            yield return new WaitForSeconds(0.5f);
            reviveViewControl.gameObject.SetActive(true);
            reviveViewControl.OnShow();

            playingViewControl.gameObject.SetActive(false);
            endGameViewControl.gameObject.SetActive(false);
        }
    }
}
