﻿using UnityEngine;
using UnityEngine.UI;

namespace CBGames
{
    public class EnvironmentViewController : MonoBehaviour
    {
        [SerializeField] private RectTransform scrollViewTrans = null;
        private EnvironmentItemController[] environmentItemControls = null;
        public void OnShow()
        {
            ViewManager.Instance.ScaleRect(scrollViewTrans, Vector2.zero, Vector2.one, 0.75f);
            if (environmentItemControls == null)
            {
                environmentItemControls = GetComponentsInChildren<EnvironmentItemController>();
            }

            foreach (EnvironmentItemController o in environmentItemControls)
            {
                o.OnSetup();
            }
        }

        private void OnDisable()
        {
            scrollViewTrans.localScale = Vector2.zero;
        }


        public void CloseEnvironmentViewBtn()
        {
            ViewManager.Instance.PlayClickButtonSound();
            ViewManager.Instance.HomeViewController.OnSubViewClose();
            gameObject.SetActive(false);
        }
    }
}
