﻿using System.Collections.Generic;
using UnityEngine;

namespace CBGames
{
    public enum IngameState
    {
        Ingame_Playing,
        Ingame_Revive,
        Ingame_GameOver,
        Ingame_CompletedLevel,
    }



    public enum IndexType
    {
        INDEX_2 = 0,
        INDEX_4 = 1,
        INDEX_8 = 2,
        INDEX_16 = 3,
        INDEX_32 = 4,
        INDEX_64 = 5,
        INDEX_128 = 6,
        INDEX_256 = 7,
        INDEX_512 = 8,
        INDEX_1024 = 9,
        INDEX_2048 = 10,
        INDEX_4096 = 11,
        INDEX_8192 = 12,
        INDEX_16384 = 13,
        INDEX_32768 = 14,
        INDEX_65536 = 15,
    }

    public enum EnvironmentType
    {
        GREEN = 0,
        BLUE = 1,
        PINK = 2,
        WHITE = 3,
        RED = 4,
    }


    [System.Serializable]
    public class LevelConfig
    {
        [Header("Level Number Configuration")]
        [SerializeField] private int minLevel = 1;
        public int MinLevel { get { return minLevel; } }
        [SerializeField] private int maxLevel = 1;
        public int MaxLevel { get { return maxLevel; } }


        [Header("Background Colors Configuration")]
        [SerializeField] private Color backgroundTopColor = Color.white;
        public Color BackgroundTopColor { get { return backgroundTopColor; } }
        [SerializeField] private Color backgroundBottomColor = Color.white;
        public Color BackgroundBottomColor { get { return backgroundBottomColor; } }


        [Header("Background Music Configuration")]
        [SerializeField] private SoundClip musicClip = null;
        public SoundClip MusicClip { get { return musicClip; } }


        [Header("Time To Complete Level Configuration")]
        [SerializeField] [Range(10, 600)] private int minTimeToCompleteLevel = 60;
        public int MinTimeToCompleteLevel { get { return minTimeToCompleteLevel; } }
        [SerializeField] [Range(10, 600)] private int maxTimeToCompleteLevel = 180;
        public int MaxTimeToCompleteLevel { get { return maxTimeToCompleteLevel; } }


        [Header("Level Target Configuration")]
        [SerializeField] private int numberOfTarget = 1;
        public int NumberOfTarget { get { return numberOfTarget; } }

        [SerializeField] private int minQuantity = 1;
        public int MinQuantity { get { return minQuantity; } }
        [SerializeField] private int maxQuantity = 1;
        public int MaxQuantity { get { return maxQuantity; } }
        [SerializeField] private IndexType[] targetIndexTypes = null;
        public IndexType[] TargetIndexTypes { get { return targetIndexTypes; } }


        [Header("Initial Merge Cubes Configuration")]
        [SerializeField] private InitialMergeCubeConfig[] initialMergeCubeConfigs = null;
        public InitialMergeCubeConfig[] InitialMergeCubeConfigs { get { return initialMergeCubeConfigs; } }


        [Header("Ingame Merge Cubes Configuration")]
        [SerializeField] private IngameMergeCubeConfig[] ingameMergeCubeConfigs = null;
        public IngameMergeCubeConfig[] IngameMergeCubeConfigs { get { return ingameMergeCubeConfigs; } }
    }


    [System.Serializable]
    public class IngameMergeCubeConfig
    {
        [SerializeField] private IndexType indexType = IndexType.INDEX_2;
        public IndexType IndexType { get { return indexType; } }
        [SerializeField] [Range(0.1f, 1f)] private float frequency = 0.5f;
        public float Frequency { get { return frequency; } }
    }

    [System.Serializable]
    public class InitialMergeCubeConfig
    {
        [SerializeField] private int quantity = 1;
        public int Quantity { get { return quantity; } }
        [SerializeField] private IndexType indexType = IndexType.INDEX_2;
        public IndexType IndexType { get { return indexType; } }
    }


    [System.Serializable]
    public class MergeCubeColorConfig
    {
        [SerializeField] private IndexType indexType = IndexType.INDEX_2;
        public IndexType IndexType { get { return indexType; } }
        [SerializeField] private Color color = Color.red;
        public Color Color { get { return color; } }
    }




    [System.Serializable]
    public class TargetSpriteConfig
    {
        [SerializeField] private IndexType indexType = IndexType.INDEX_1024;
        public IndexType IndexType { get { return indexType; } }
        [SerializeField] private Sprite targetSprite = null;
        public Sprite TargetSprite { get { return targetSprite; } }
    }



    [System.Serializable]
    public class EnvironmentConfig
    {
        [SerializeField] private EnvironmentType environmentType = EnvironmentType.GREEN;
        public EnvironmentType EnvironmentType { get { return environmentType; } }
        [SerializeField] private GameObject tableObject = null;
        public GameObject TableObject { get { return tableObject; } }
    }





    [System.Serializable]
    public class TargetData
    {
        public int Quantity { private set; get; }
        public void SetQuantity(int quantity)
        {
            Quantity = quantity;
        }
        public IndexType IndexType { private set; get; }
        public void SetIndexType(IndexType indexType)
        {
            IndexType = indexType;
        }
    }


    public class PlayerLeaderboardData
    {
        public string Name { private set; get; }
        public void SetName(string name)
        {
            Name = name;
        }

        public int Level { private set; get; }
        public void SetLevel(int level)
        {
            Level = level;
        }
    }
}
