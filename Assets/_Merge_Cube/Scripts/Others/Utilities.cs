﻿using System;
using UnityEngine;

namespace CBGames
{
    public class Utilities
    {
        /// <summary>
        /// Covert the given seconds to time format.
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static string SecondsToTimeFormat(double seconds)
        {
            int hours = (int)seconds / 3600;
            int mins = ((int)seconds % 3600) / 60;
            seconds = Math.Round(seconds % 60, 0);
            return hours + ":" + mins + ":" + seconds;
        }

        /// <summary>
        /// Covert the given seconds to minutes format.
        /// </summary>
        /// <param name="seconds"></param>
        /// <returns></returns>
        public static string SecondsToMinutesFormat(double seconds)
        {
            int mins = ((int)seconds % 3600) / 60;
            seconds = Math.Round(seconds % 60, 0);
            return mins + ":" + seconds;
        }

        /// <summary>
        /// Convert color to hex
        /// </summary>
        /// <param name="color"></param>
        /// <returns></returns>
        public static string ColorToHex(Color32 color)
        {
            string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
            return hex;
        }

        /// <summary>
        /// Convert hex to color
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static Color HexToColor(string hex)
        {
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            return new Color32(r, g, b, 255);
        }




        /// <summary>
        /// Get next indx type base on given index type.
        /// </summary>
        /// <param name="inputIndexType"></param>
        /// <returns></returns>
        public static IndexType GetNextIndexType(IndexType inputIndexType)
        {
            switch (inputIndexType)
            {
                case IndexType.INDEX_2:
                    return IndexType.INDEX_4;
                case IndexType.INDEX_4:
                    return IndexType.INDEX_8;
                case IndexType.INDEX_8:
                    return IndexType.INDEX_16;
                case IndexType.INDEX_16:
                    return IndexType.INDEX_32;
                case IndexType.INDEX_32:
                    return IndexType.INDEX_64;
                case IndexType.INDEX_64:
                    return IndexType.INDEX_128;
                case IndexType.INDEX_128:
                    return IndexType.INDEX_256;
                case IndexType.INDEX_256:
                    return IndexType.INDEX_512;
                case IndexType.INDEX_512:
                    return IndexType.INDEX_1024;
                case IndexType.INDEX_1024:
                    return IndexType.INDEX_2048;
                case IndexType.INDEX_2048:
                    return IndexType.INDEX_4096;
                case IndexType.INDEX_4096:
                    return IndexType.INDEX_8192;
                case IndexType.INDEX_8192:
                    return IndexType.INDEX_16384;
                case IndexType.INDEX_16384:
                    return IndexType.INDEX_32768;
                case IndexType.INDEX_32768:
                    return IndexType.INDEX_65536;
                default:
                    return IndexType.INDEX_65536;
            }
        }
    }
}
