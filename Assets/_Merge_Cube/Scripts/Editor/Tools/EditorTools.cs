﻿using UnityEngine;
using UnityEditor;

namespace CBGames
{
    public class EditorTools : EditorWindow
    {


        [MenuItem("Tools/Reset PlayerPrefs")]
        public static void ResetPlayerPrefs()
        {
            PlayerPrefs.DeleteAll();
            Debug.Log("*************** PlayerPrefs Was Deleted ***************");
        }


        [MenuItem("Tools/Capture Screenshot")]
        public static void CaptureScreenshot()
        {
            string path = "C:/Users/TienNQ/Desktop/icon.png";
            ScreenCapture.CaptureScreenshot(path);
        }
    }
}
